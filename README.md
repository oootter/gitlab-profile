# :otter: Как поднять

## [mocks](https://gitlab.com/oootter/mocks)
API, выдающее заранее заготовленные файлы.

Зависимости устанавливаются через `pip install -r requirements.txt`. 

В директории, задаваемой переменной окружения `DATASET_PATH` (по умолчанию `../dataset`) должны лежать файлы `ships.json`, `docks.json`, `schedule.json`. В ней же должен лежать файл с телеметрией, его название по умолчанию `telemetry.json`, можно задать другое переменной окружения `TELEMETRY_FILE`.

По умолчанию работает на `127.0.0.1:2408`, это можно изменить переменными окружения `SERVE_HOST` и `SERVE_PORT`.

Запуск: `python main.py`

## [gtfs-rt-exporter](https://gitlab.com/oootter/driveit)
Подключается к `mocks`. HTTP-сервер, отдающий GTFS-RT фид по `GET /feed.pb`.

Зависимости устанавливаются через `pip install -r requirements.txt`. 

В `gtfs_rt_exporter/config.py` может понадобиться изменить хосты в `SHIP_API` и `TELEMTRY_API` в соответствии с тем, где поднят `mocks`.

Запуск: `python -m uvicorn main:app --host {HOST} --port {PORT}`. По умолчанию HOST `127.0.0.1`.

## [gtfs-converter](https://gitlab.com/oootter/gtfs-converter)
Подключается к `mocks`. HTTP-сервер, отдающий зипку с GTFS по `GET /gtfs_feed`.

Настройки вынесены в config.yaml, который должен находиться в корне проекта. Также необходимо создать в корне папку `gtfs`. 

Зависимости ставятся командой `go get ./...`. После этого запуск: `go run ./cmd/main.go`.

## [visualizer](https://gitlab.com/oootter/visualizer)
Фронтенд.

Установка зависимостей: `npm install`

Сборка для отладки: `npm run build_debug`

Сборка для прода: `npm run build`

После сборки все нужные файлы появляются в `dist/`, эту директорию можно сервить как статические файлы (например, через nginx: `alias /path/to/visualizer/dist;`)
